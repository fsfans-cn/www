<?php
/*
 * This file is released into the public domain, marked with CC0.
 * A copy of the CC0 Public Domain Dedication can be found on:
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

const NAVBAR = [
	[
		'title' => 'About',
		'link' => '/about/'
	],
	[
		'title' => 'Learn',
		'link' => 'https://learn.fsfans.club/'
	],
	[
		'title' => 'Posts',
		'link' => '/posts/'
	],
	[
		'title' => 'Community',
		'link' => '/community/'
	],
	[
		'title' => 'Wiki',
		'link' => 'https://wiki.fsfans.club/'
	]
];

function in_dir($child, $parent) {
	return str_starts_with(rtrim($child, '/') . '/', rtrim($parent, '/') . '/');
}

function split($parsed) {
	$hrtag = '<hr />';
	$hrpos = strrpos($parsed, $hrtag);
	if ($hrpos !== false) {
		return [
			substr($parsed, 0, $hrpos),
			substr($parsed, $hrpos + strlen($hrtag))
		];
	}
	return [$parsed, ''];
}
