<?php
/*
 * This file is released into the public domain, marked with CC0.
 * A copy of the CC0 Public Domain Dedication can be found on:
 * <https://creativecommons.org/publicdomain/zero/1.0/>.
 */

$is_root_index = ($dir_relative == '' && $basename == 'index.md');

[$main, $footer] = split($parsed);
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php if (!$is_root_index) echo $frontmatter['title'] . ' - '; ?>Free Software Fans</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="/css/main.css" rel="stylesheet">
	<?php if (isset($frontmatter['style'])) { ?>
		<link href="/css/<?= $frontmatter['style'] ?>" rel="stylesheet">
	<?php } ?>
</head>

<body>
	<div id="top">
		<a class="skip" href="#content"><b>Skip to main text</b></a>
	</div>

	<div id="header">
		<div id="banner">
			<a href="/">
				<svg width="64px" height="64px"><use xlink:href="/res/fsfans-logo.svg#logo"></svg>
				<svg width="360px" height="64px" aria-label="Free Software Fans"><use xlink:href="/res/fsfans-arttype.svg#fsfans"></svg>
			</a>
			<br />
			<small>A free software community in China</small>
		</div>

		<ul id="nav">
			<li>
				<a href="#nav">≫</a>
				<a href="#content">⨉</a>
			</li><?php
			foreach (NAVBAR as $item) {
				$class = in_dir($dir_relative, $item['link']) ? 'dim' : '' ?>
				<li>
					<a href="<?= $item['link'] ?>" class="<?= $class ?>"><?= $item['title'] ?></a>
				</li><?php
			} ?>
		</ul>
	</div>

	<main id="content">
		<?= $main ?>
	</main>

	<footer>
		<div class="back-to-top"><a href="#top">▲</a></div>
		<?= $footer ?>
		<p>Free Software Fans (FSFans) is not affiliated with the Free Software Foundation or the GNU Project.</p>
	</footer>
</body>
</html>
