# Membership Approval Procedures for FSFans

To clarify of the membership application process, we implemented the following procedures:

Applicants must submit their applications to the designated email. Upon receipt, the admin will create a wiki page containing a copy of the application. Both existing FSFans members and the applicant can comment and discuss on the wiki page. A poll will be conducted alongside the discussion, and the application will be approved if the number of "OK" votes minus "NOT OK" votes equals or exceeds 3. ((ok_cnt - notok_cnt) >= 3)

The voting process will conclude within 48 hours.

Any rejected application may be resubmitted after at least one month.

**Effective as of 2025-02-04.**

# FSFans 成员审批流程

为了明确成员申请流程，我们实施了以下程序：

申请人必须将申请提交至指定邮箱。收到申请后，管理员将创建一个包含申请副本的维基页面。现有的 FSFans 成员和申请人均可在该页面上评论和讨论。同时将进行投票，若“同意”票数减去“不同意”票数等于或超过3票 ((ok_cnt - notok_cnt) >= 3)，则申请通过。

投票过程将在48小时内结束。

任何被拒绝的申请可在至少一个月后重新提交。

**自 2025 年 2 月 4 日起生效。**

---

Copyright (C) 2025 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
