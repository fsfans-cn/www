# FSFans 作为合作社区参加 COSCon'24

COSCon'24 第九届中国开源年会暨开源社十周年嘉年华，于 2024 年 11 月 2-3 日在中关村国家自主创新示范区会议中心举办。

本次中国开源年会恰逢开源社 10 周年，众多社区将共襄盛举，联合带来不同主题方向的专题论坛，涵盖 FLOSS 评价标准、FLOSS 治理、FLOSS 人才教育等方向。

Free Software Fans 也作为合作社区参与 COSCon'24，与其它 FLOSS 社区同乐。

![现场照片](fsfans-club.jpg)

![现场照片](gnu-org.jpg)

---

Copyright (C) 2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
