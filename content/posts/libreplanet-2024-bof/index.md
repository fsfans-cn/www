# LibrePlanet 2024 BoF

5 月 3 日，FSFans 在北京市通州区大运河森林公园举办 [LibrePlanet 2024 BoF](https://libreplanet.org/wiki?title=LibrePlanet:Conference/2024/BOF#Free_Software_in_Public_Services_&_Daily_Life) 活动。

![现场照片](lp2024bof_2512185371.jpg)

![现场照片](lp2024bof_630329302.jpg)

---

Copyright (C) 2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
