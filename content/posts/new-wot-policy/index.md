# WoT 政策更新

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

新的 WoT 政策
=============================

自 2024 年 8 月 12 日起，社区所有成员必须提供经过签名并验证的 PGP 密钥，以及密钥的
URL。未持有有效密钥的成员，将无法邀请新成员加入社区。新成员在加入社区之前，必须先将其
经过签名的 PGP 密钥及其 URL 发布至我们的 WoT 频道。若发现任何成员持有无效或未经验证
的密钥，需立即向管理员报告。

有关使用 PGP 和 WoT 的更多信息，请参见
 - https://emailselfdefense.fsf.org/

Announcing our New WoT Policy
=============================

Effective August 12, 2024, all members of the community must provide a signed
and verified PGP key along with a URL to their key. Any member without a valid
one will not be able to invite new members to the community. For new members, a
signed PGP key and its URL must be published in our WoT channel before they can
join the community. Any member found with invalid or unverified keys should be
reported to the administrators immediately.

For more information on working with PGP and WoT, refer to
 - https://emailselfdefense.fsf.org/
-----BEGIN PGP SIGNATURE-----

iHUEARYIAB0WIQSlfwowy5lyPxrECWqEqEefdeneIQUCZrfkfwAKCRCEqEefdene
IXfiAQCrlXMFX4Xf95QQUFpzvGJ9gipBQFZ+7402B1SioKrQUAEAg6dqSBqv9OEm
ejuC4xljQq781pI//uoKkOd4VY4b7gE=
=OMRT
-----END PGP SIGNATURE-----
```

---

Copyright (C) 2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
