# 2023 春节 Minetest 合照

![Minetest Screenshot](screenshot_20230121_213134.png)

---

Copyright (C) 2023 William Goodspeed

This page is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
