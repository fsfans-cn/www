# 2024 deepin Meetup 闪电演讲

1 月 27 日，William Goodspeed 在 deepin Meetup 2024 北京站发表关于自由软件运动的闪电演讲。

![现场照片](lightning-talk.png)

## 链接

- [幻灯片及其源文件](slides.tgz)（1.2 MB）
- 视频：[WebM](https://files.fsfans.club/2024-deepin-meetup-talk.webm)（55 MB），[MP4](https://files.fsfans.club/2024-deepin-meetup-talk-original.mp4)（1.3 GB）

## 后记

FSFans 成员参与各类他方活动，不代表我们对该活动或其相关组织背书。

---

Copyright (C) 2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
