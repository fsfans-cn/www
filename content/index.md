---
{ "style": "home.css" }
---

<section>
<div>

# Promoting Digital Freedom

Free Software Fans (FSFans) is a [Free Software](https://learn.fsfans.club/) community.
We promote software that respects user freedom and privacy.

</div>
<svg><use xlink:href="res/surveillance-camera.svg#nosurveillance"></svg>
</section>


<section>
<div>

# Hacking for Fun

Making up FSFans are people who enjoy hacking and tech.

</div>
<svg><use xlink:href="res/laptop.svg#laptop"></svg>
</section>


<section>
<div>

# Sharing for the Common Good

We at FSFans love sharing with each other what's really awesome,
from an Emacs configuration to a chicken soup recipe.

</div>
<svg><use xlink:href="res/conversation.svg#conversation"></svg>
</section>


<section>
<div>

# Gaming Together

Feeling bored? How about playing some video games?
FSFans has a number of game servers and a VoIP chat server.

</div>
<svg><use xlink:href="res/game.svg#game"></svg>
</section>


<section class="join-us">
<div>

# Wondering more or feeling like joining?

[Learn More](/about/)
[Join Us](/community/)

</div>
</section>

---

Copyright (C) 2023-2024 FSFans.club

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
