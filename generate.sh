if ! command -v php >/dev/null 2>&1; then
    echo "PHP is not installed. Installing php-cli and php-dom..."
    apt update && apt install -y php-cli php-dom php-mbstring
fi

if ! command -v git >/dev/null 2>&1; then
    echo "GIT is not installed. Installing git..."
    apt update && apt install -y git
fi

SRC_DIR="$(dirname "$(realpath "$0")")"
SSG_DIR="./peaksol-org-ssg"
OUT_DIR="./out"

if [ ! -d "${SSG_DIR}" ]; then
    echo "Directory ${SSG_DIR} not found. Cloning repository..."
    mkdir -p temp
    git clone --depth=1 https://codeberg.org/Peaksol/peaksol-org-ssg.git "${SSG_DIR}"
fi

php "${SSG_DIR}/peaksol-org-ssg.php" \
    --template "${SRC_DIR}/template" \
    --input "${SRC_DIR}/content" \
    --output "$OUT_DIR" \
    --clean
