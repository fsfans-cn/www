# Notes for Webmasters

This site uses `peaksol-org-ssg` as its static site generator. User guide is available [here](https://codeberg.org/Peaksol/peaksol-org-ssg).

Here is a quick reference for you to perform common operations to the site.

## Creating a page

Create a `.md` file in `contents/` and write whatever you want.

To use a custom title rather than the one automatically generated, start the file with:

```
---
{"title": "Lorem Ipsum"}
---
```

To add a copyright notice, end the file with:

```md
---

Copyright (C) [YEAR] [YOUR NAME]

This page is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
```

## Static files

Static files can be placed in `content/`. If they are especially for web styling rather than the content, place them in `template/assets/`.

## Generating the site

`generate.sh` will install related dependencies and build the site for you:

```sh
./generate.sh
```

It will output to `./out`. But please also note that, it will only complete the dependencies for apt based system. If you are not using those, manually install the packages `php-cli php-dom php-mbstring git`.

## Publishing the site

If you have write access to the repository, you could run `publish.sh` to publish the site or push your changes and let CI do its job. Otherwise, you need to create a pull request and then let Forgejo CI or other webmasters to publish for you.
